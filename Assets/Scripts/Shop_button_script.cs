﻿using UnityEngine;
using UnityEngine.UI;

public class Shop_button_script : MonoBehaviour
{

    //public Text coin_value;
    public Text Price;
    public int price_value;
    //public string priceeee;
    public GameObject buy_button;
    public GameObject Set;
    public GameObject equip;
    public string ID;

    public Image COIN_TYPE_IMG;


    public string shop_id;
    public coin_type ct;

    public shop_type shop;

    public void OnEnable()
    {
        Shop_panel_script.update_buttons += update_but;
        //Shop_panel_script.Instance.update_shop_but();
        ONCLICK_but();
    }

    public void OnDisable()
    {
        Shop_panel_script.update_buttons -= update_but;
    }


    private void Awake()
    {
        //PlayerPrefs.SetInt(ID, )

        //if(0)
    }
    // Start is called before the first frame update
    void Start()
    {
        Price.text = price_value.ToString();
        if (ID == "default")
        {
            PlayerPrefs.SetInt(ID, 1);
        }
        switch (ct)
        {
            case coin_type.Diamond:
                COIN_TYPE_IMG.sprite = Shop_panel_script.Instance.diamond;
                break;
            case coin_type.Gold:
                COIN_TYPE_IMG.sprite = Shop_panel_script.Instance.gold;
                break;
        }
        switch (shop)
        {
            case shop_type.Cloth:
                shop_id = "Cloth_Equip";
                break;
            case shop_type.Bg:
                shop_id = "Bg_Equip";
                break;
            case shop_type.Cannon:
                shop_id = "Cannon_Equip";
                break;
        }
        update_but();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void ONCLICK_but()
    {
        buy_button.GetComponent<Button>().onClick.AddListener(() => On_buy_fun());
        Set.GetComponent<Button>().onClick.AddListener(() => On_Set_fun());
    }

    public void On_buy_fun()
    {
        switch (ct)
        {
            case coin_type.Diamond:
                if (float.Parse(Price.text) <= PlayerPrefs.GetFloat("Diamonds"))
                {
                    float s = PlayerPrefs.GetFloat("Diamonds") - float.Parse(Price.text);
                    PlayerPrefs.SetFloat("Diamonds", s);

                    Shop_panel_script.Instance.buy_item(ID, shop);
                }
                else
                {
                    not_enough_money();
                }
                break;
            case coin_type.Gold:
                if (int.Parse(Price.text) <= PlayerPrefs.GetInt("Gems"))
                {
                    int s = PlayerPrefs.GetInt("Gems") - int.Parse(Price.text);
                    PlayerPrefs.SetInt("Gems", s);
                    Shop_panel_script.Instance.buy_item(ID, shop);
                }
                else
                {
                    not_enough_money();
                }
                break;
        }

    }


    public void not_enough_money()
    {
        GameManager.Instance.nem_popup_fun("Not Enough Coins!!!");
    }
    public void On_Set_fun()
    {
        PlayerPrefs.SetString(shop_id, ID);
        Shop_panel_script.Instance.set_item(ID,shop);
        Shop_panel_script.Instance.update_shop_but();
    }

    public void update_but()
    {
        //BUY
        if (PlayerPrefs.GetInt(ID, 0) == 0)
        {
            Set.SetActive(false);
            buy_button.SetActive(true);
            equip.SetActive(false);
        }
        else
        {
            //equip
            if (PlayerPrefs.GetString(shop_id, "default") == ID)
            {
                Set.SetActive(false);
                buy_button.SetActive(false);
                equip.SetActive(true);
            }
            //set
            else
            {
                Set.SetActive(true);
                buy_button.SetActive(false);
                equip.SetActive(false);
            }

        }
    }

}
