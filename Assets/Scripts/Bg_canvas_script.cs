﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bg_canvas_script : MonoBehaviour
{
    public static Bg_canvas_script Instance;

    public Sprite[] BG_sprites;
    public Sprite[] BG_front_sprites;

    public Image BG_image;

    public Image BG_front;


    private void Awake()
    {
        Instance = this;
    }




    // Start is called before the first frame update
    void Start()
    {
        set_bg();
        //ipad and iphone resolution set
        //if (((float)Screen.height / (float)Screen.width) < 1.7)
        //{
        //    BG_front.sprite = BG_front_sprites[1];
        //}
        //else
        //{
        //    BG_front.sprite = BG_front_sprites[0];
        //}
        if (((float)Screen.height / (float)Screen.width) < 1.7)
        {
            //ipad
            BG_front.sprite = BG_front_sprites[1];
        }
        else if (((float)Screen.height / (float)Screen.width) > 2)
        {
            //iphone x
            BG_front.sprite = BG_front_sprites[2];
        }
        else
        {
            //iphone
            BG_front.sprite = BG_front_sprites[0];
        }


    }

    public void set_bg()
    {
        string amd = PlayerPrefs.GetString("Bg_Equip","default");
        if(amd != "default")
        {
            string s = amd.Substring(3);
            BG_image.sprite = Canvas_singleton_script.Instance.bg_img_array[int.Parse(s)];
        }
        else
        {
            BG_image.sprite = Canvas_singleton_script.Instance.bg_img_array[0];
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
