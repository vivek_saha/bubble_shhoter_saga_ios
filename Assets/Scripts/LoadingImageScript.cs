using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingImageScript : MonoBehaviour
{
	private void Start()
	{
		base.StartCoroutine("GiftCard_Animplay");
	}


	public void start_enim()
    {
		base.StartCoroutine("GiftCard_Animplay");
	}
	private IEnumerator GiftCard_Animplay()
	{
		int i = 0;
		for (;;)
		{
			yield return new WaitForSecondsRealtime(0.1f);
			if (i < this.Sprites.Count)
			{
				base.transform.GetComponent<Image>().sprite = return_load_sprite(i);
				i++;
			}
			else
			{
				i = 0;
				base.StartCoroutine("GiftCard_Animplay");
			}
		}
		yield break;
	}

	public List<Sprite> Sprites = new List<Sprite>();
	public List<Sprite> Brown_Sprites = new List<Sprite>();
	public List<Sprite> Orange_Sprites = new List<Sprite>();
	public List<Sprite> Pink_Sprites = new List<Sprite>();
	public List<Sprite> Purple_Sprites = new List<Sprite>();
	public List<Sprite> Yellow_Sprites = new List<Sprite>();



    public Sprite return_load_sprite(int i)
    {
        switch (PlayerPrefs.GetString("Cloth_Equip", "default"))
        {
			case "default":
				//return Sprites[i];
				return Orange_Sprites[i];
				break;
			case "cloth_1":
				return Brown_Sprites[i];
				break;
			case "cloth_2":
				return Orange_Sprites[i];
				break;
			case "cloth_3":
				return Pink_Sprites[i];
				break;
			case "cloth_4":
				return Purple_Sprites[i];
				break;
			case "cloth_5":
				return Yellow_Sprites[i];
				break;

			default:
				//return Sprites[i];
				return Orange_Sprites[i];
				break;
        }
    }
}
