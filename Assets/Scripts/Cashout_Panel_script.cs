﻿using UnityEngine;
using UnityEngine.UI;

public class Cashout_Panel_script : MonoBehaviour
{
    public static Cashout_Panel_script Instance;
    public Image Cashout_but;

    public Sprite ready_cashout, proccesing_cashout;

    public GameObject nem_popup;

    public RectTransform origin;

    [Space]
    public Text Coin_text;
    public Text bit_coin_text;
    public Text Panel_msg;
    public Image slider_image;
    public GameObject google_sign_in_but;
    public GameObject Slider_object;
    public GameObject Cashout_panel;


    public GameObject character;

    public GameObject redeem_but, withdrawal_but;
    public GameObject shop_but, rateus_but;

    private void Awake()
    {
        Instance = this;
    }

    
    // Start is called before the first frame update
    void Start()
    {
        play_homescreen_anim();
        if (Setting_API.Instance.redeem_skin_enabled)
        {
            redeem_but.SetActive(true);
            withdrawal_but.SetActive(true);
            shop_but.SetActive(false);
            rateus_but.SetActive(false);
        }
        else
        {
            redeem_but.SetActive(false);
            withdrawal_but.SetActive(false);
        }
        //but_change();
    }
    
    private void OnEnable()
    {
        play_homescreen_anim();
        if (Setting_API.Instance.redeem_skin_enabled)
        {
            redeem_but.SetActive(true);
            withdrawal_but.SetActive(true);
            shop_but.SetActive(false);
            rateus_but.SetActive(false);
        }
        else
        {
            redeem_but.SetActive(false);
            withdrawal_but.SetActive(false);
        }
    }

    //public void but_change()
    //{
    //    if (Setting_API.Instance.is_cashout_done)
    //    {
    //        Cashout_but.sprite = proccesing_cashout;
    //    }
    //    else
    //    {
    //        Cashout_but.sprite = ready_cashout;
    //    }
    //}

    // Update is called once per frame
    void Update()
    {

    }

    public void nem_popup_fun(string msg)
    {
        GameObject a = Instantiate(nem_popup);
        a.GetComponent<nem_popup_script>().msg.text = msg;
        a.transform.SetParent(this.transform);
        a.transform.localScale = Vector3.one;
        a.transform.position = origin.transform.position;
    }

    public void Onbut_click()
    {
        if (Cashout_but.sprite == proccesing_cashout)
        {
            nem_popup_fun("Processing..." +
                "\nIt can take 24-48h");
        }
        else
        {
            start_cashout_panel();
        }
    }

    public void start_cashout_panel()
    {
        google_sign_in_but.SetActive(false);
        Panel_msg.gameObject.SetActive(false);
        Slider_object.SetActive(false);

        //Panel_msg.text = "You need to have atleast " + Setting_API.Instance.cashout_coin_value.ToString() +" coins to cashout!";
        //Coin_text.text = PlayerPrefs.GetInt("Gems", 0).ToString();
        //bit_coin_text.text = Setting_API.Instance.bit_coin_value + " Bitcoin";
        //if (PlayerPrefs.GetInt("Gems") == 0)
        //{
        //    slider_image.fillAmount = 0;

        //}
        //else
        //{
        //    slider_image.fillAmount = (float)PlayerPrefs.GetInt("Gems") /(float) Setting_API.Instance.cashout_coin_value;
        //    Debug.Log("slider_image.fillAmount " + slider_image.fillAmount);
        //}
        //if (slider_image.fillAmount >= 1)
        //{
        //    google_sign_in_but.SetActive(true);
        //}
        //else
        //{

        //    Panel_msg.gameObject.SetActive(true);
        //    Slider_object.SetActive(true);
        //}
        Cashout_panel.SetActive(true);
    }


    public void play_homescreen_anim()
    {
        switch (PlayerPrefs.GetString("Cloth_Equip", "default"))
        {
            case "default":
                //character.GetComponent<Animator>().Play("home_play_but_anim");
                character.GetComponent<Animator>().Play("Orange_home_play_but_anim");
                //return Sprites[i];
                break;
            case "cloth_1":
                character.GetComponent<Animator>().Play("Brown_home_play_but_anim");
                //return Brown_Sprites[i];
                break;
            case "cloth_2":
                character.GetComponent<Animator>().Play("Orange_home_play_but_anim");
                //return Orange_Sprites[i];
                break;
            case "cloth_3":
                character.GetComponent<Animator>().Play("Pink_home_play_but_anim");
                //return Pink_Sprites[i];
                break;
            case "cloth_4":
                character.GetComponent<Animator>().Play("Purple_home_play_but_anim");
                //return Purple_Sprites[i];
                break;
            case "cloth_5":
                character.GetComponent<Animator>().Play("Yellow_home_play_but_anim");
                //return Yellow_Sprites[i];
                break;

            default:
                character.GetComponent<Animator>().Play("home_play_but_anim");
                //return Sprites[i];
                break;

        }

    }



    public void Rate_us()
    {
        Application.OpenURL(Setting_API.Instance.ios_app_link);
    }

    //public void OnCLick_google_login()
    //{
    //    GoogleSignInDemo.Instance.SignInWithGoogle();
    //}
}
