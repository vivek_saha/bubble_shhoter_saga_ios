﻿using UnityEngine;

public enum n_panel_type { No_internet_Panel, Update_Panel, Undermaintenance_panel, Something_wentwrong_panel, Loading_panel, Blocked_panel }

public class Notice_panel_script : MonoBehaviour
{
    public static Notice_panel_script Instance;
    public GameObject No_internet_Panel, Update_Panel, Undermaintenance_panel, Something_wentwrong_panel, Loading_panel, Blocked_panel;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void Start_notice_panel(n_panel_type type)
    {
        switch (type)
        {
            case n_panel_type.No_internet_Panel:
                break;

            case n_panel_type.Update_Panel:
                break;

            case n_panel_type.Undermaintenance_panel:
                break;

            case n_panel_type.Something_wentwrong_panel:
                break;

            case n_panel_type.Loading_panel:
                break;

            case n_panel_type.Blocked_panel:
                break;

            default:
                break;
        }
    }

    public void Close_notice_panel()
    {
        No_internet_Panel.SetActive(false);
        Update_Panel.SetActive(false);
        Undermaintenance_panel.SetActive(false);
        Something_wentwrong_panel.SetActive(false);
        Loading_panel.SetActive(false);
        Blocked_panel.SetActive(false);
        gameObject.SetActive(false);
    }
}