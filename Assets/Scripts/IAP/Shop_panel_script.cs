﻿using UnityEngine;

public enum shop_type { Cloth, Bg, Cannon }
public class Shop_panel_script : MonoBehaviour
{
    public static Shop_panel_script Instance;
    //public GameObject Scroll_content;

    //public GameObject  combo1,Ads_pop;

    public delegate void Update_shop_but();
    public static event Update_shop_but update_buttons;

    public GameObject cloth_panel, bg_panel, cannon_panel;
    public GameObject cloth_content;
    public GameObject bg_content;
    public GameObject cannon_content;

    public Sprite gold, diamond;
    //public GameObject shoppanel;
    private void Awake()
    {
        Instance = this;
    }


    public void Onclick_panel_change_(GameObject tp)
    {
        cloth_panel.SetActive(false);
        bg_panel.SetActive(false);
        cannon_panel.SetActive(false);
        tp.SetActive(true);
    }

    private void OnEnable()
    {
        update_shop_but();
    }
    // Start is called before the first frame update
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {

    }

    public void On_shop_panel()
    {
        //shoppanel.SetActive(true);
    }

    //public void Set_shop(string iid)
    //{
    //    PlayerPrefs.SetString(shop_id, "default"
    //}

    public void buy_item(string id, shop_type s)
    {
        PlayerPrefs.SetInt(id, 1);
        set_item(id, s);
        update_shop_but();
    }

    public void set_item(string id, shop_type s)
    {
        switch (s)
        {
            case shop_type.Cloth:
                PlayerPrefs.SetString("Cloth_Equip", id);
                play_homescreen_anim();
                break;
            case shop_type.Bg:
                PlayerPrefs.SetString("Bg_Equip", id);
                Bg_canvas_script.Instance.set_bg();
                break;
            case shop_type.Cannon:
                PlayerPrefs.SetString("Cannon_Equip", id);
                break;
            default:
                break;
        }
    }

    public void update_shop_but()
    {
        update_buttons();
    }


    public void play_homescreen_anim()
    {
        switch (PlayerPrefs.GetString("Cloth_Equip", "default"))
        {
            case "default":
                //Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("home_play_but_anim");
                Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("Orange_home_play_but_anim");
                //return Sprites[i];
                break;
            case "cloth_1":
                Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("Brown_home_play_but_anim");
                //return Brown_Sprites[i];
                break;
            case "cloth_2":
                Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("Orange_home_play_but_anim");
                //return Orange_Sprites[i];
                break;
            case "cloth_3":
                Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("Pink_home_play_but_anim");
                //return Pink_Sprites[i];
                break;
            case "cloth_4":
                Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("Purple_home_play_but_anim");
                //return Purple_Sprites[i];
                break;
            case "cloth_5":
                Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("Yellow_home_play_but_anim");
                //return Yellow_Sprites[i];
                break;

            default:
                Cashout_Panel_script.Instance.character.GetComponent<Animator>().Play("home_play_but_anim");
                //return Sprites[i];
                break;

        }

    }
}