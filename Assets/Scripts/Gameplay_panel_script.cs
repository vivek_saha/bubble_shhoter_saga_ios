﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gameplay_panel_script : MonoBehaviour
{

    public static Gameplay_panel_script Instance;

    public Text Level_diamond;


    public GameObject chaaracter_anim;

    public GameObject redeem_but, withdrawal_but;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        play_idel_anim();
        if (((float)Screen.height / (float)Screen.width) < 1.7f)
        {
            Debug.Log("HERE");
            //ipad
            chaaracter_anim.GetComponent<RectTransform>().anchoredPosition = new Vector3(-180f, -300f, 0f);
            chaaracter_anim.GetComponent<RectTransform>().sizeDelta = new Vector2(190, 200);
        }
        if (Setting_API.Instance.redeem_skin_enabled)
        {
            redeem_but.SetActive(true);
            withdrawal_but.SetActive(true);
        }
        else
        {
            redeem_but.SetActive(false);
            withdrawal_but.SetActive(false);
        }
    }

    private void OnEnable()
    {
        play_idel_anim();
        if (((float)Screen.height / (float)Screen.width) < 1.7f)
        {
            Debug.Log("HERE");
            //ipad
            chaaracter_anim.GetComponent<RectTransform>().anchoredPosition = new Vector3(-180f, -300f, 0f);
            chaaracter_anim.GetComponent<RectTransform>().sizeDelta = new Vector2(190, 200);
        }
        if (Setting_API.Instance.redeem_skin_enabled)
        {
            redeem_but.SetActive(true);
            withdrawal_but.SetActive(true);
        }
        else
        {
            redeem_but.SetActive(false);
            withdrawal_but.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Withdraw_panel()
    {
        Canvas_singleton_script.Instance.Top_panel.SetActive(true);
        Canvas_singleton_script.Instance.Withdraw_panel.SetActive(true);
    }
    public void Redeem_panel()
    {
        Canvas_singleton_script.Instance.Top_panel.SetActive(true);
        Canvas_singleton_script.Instance.Redeem_panel.SetActive(true);
    }

    public void play_idel_anim()
    {
        switch (PlayerPrefs.GetString("Cloth_Equip", "default"))
        {
            case "default":
                //chaaracter_anim.GetComponent<Animator>().Play("charcater_idel");
                chaaracter_anim.GetComponent<Animator>().Play("Orange_character_idel_anim");
                //return Sprites[i];
                break;
            case "cloth_1":
                chaaracter_anim.GetComponent<Animator>().Play("Brown_character_idel_anim");
                //return Brown_Sprites[i];
                break;
            case "cloth_2":
                chaaracter_anim.GetComponent<Animator>().Play("Orange_character_idel_anim");
                //return Orange_Sprites[i];
                break;
            case "cloth_3":
                chaaracter_anim.GetComponent<Animator>().Play("Pink_character_idel_anim");
                //return Pink_Sprites[i];
                break;
            case "cloth_4":
                chaaracter_anim.GetComponent<Animator>().Play("Purple_character_idel_anim");
                //return Purple_Sprites[i];
                break;
            case "cloth_5":
                chaaracter_anim.GetComponent<Animator>().Play("Yellow_character_idel_anim");
                //return Yellow_Sprites[i];
                break;

            default:
                chaaracter_anim.GetComponent<Animator>().Play("Orange_character_idel_anim");
                //return Sprites[i];
                break;

        }


    }

    public void play_hit_anim()
    {
        switch (PlayerPrefs.GetString("Cloth_Equip", "default"))
        {
            case "default":
                //chaaracter_anim.GetComponent<Animator>().Play("Play_hit_animation");
                chaaracter_anim.GetComponent<Animator>().Play("Orange_play_hit_anim");
                //return Sprites[i];
                break;
            case "cloth_1":
                chaaracter_anim.GetComponent<Animator>().Play("Brown_play_hit_anim");
                //return Brown_Sprites[i];
                break;
            case "cloth_2":
                chaaracter_anim.GetComponent<Animator>().Play("Orange_play_hit_anim");
                //return Orange_Sprites[i];
                break;
            case "cloth_3":
                chaaracter_anim.GetComponent<Animator>().Play("Pink_play_hit_anim");
                //return Pink_Sprites[i];
                break;
            case "cloth_4":
                chaaracter_anim.GetComponent<Animator>().Play("Purple_play_hit_anim");
                //return Purple_Sprites[i];
                break;
            case "cloth_5":
                chaaracter_anim.GetComponent<Animator>().Play("Yellow_play_hit_anim");
                //return Yellow_Sprites[i];
                break;

            default:
                chaaracter_anim.GetComponent<Animator>().Play("Orange_play_hit_anim");
                //return Sprites[i];
                break;

        }


    }
    public void win_anim()
    {
        switch (PlayerPrefs.GetString("Cloth_Equip", "default"))
        {
            case "default":
                //chaaracter_anim.GetComponent<Animator>().Play("win_character_anim");
                chaaracter_anim.GetComponent<Animator>().Play("Orange_win_character_anim");
                //return Sprites[i];
                break;
            case "cloth_1":
                chaaracter_anim.GetComponent<Animator>().Play("Brown_win_character_anim");
                //return Brown_Sprites[i];
                break;
            case "cloth_2":
                chaaracter_anim.GetComponent<Animator>().Play("Orange_win_character_anim");
                //return Orange_Sprites[i];
                break;
            case "cloth_3":
                chaaracter_anim.GetComponent<Animator>().Play("Pink_win_character_anim");
                //return Pink_Sprites[i];
                break;
            case "cloth_4":
                chaaracter_anim.GetComponent<Animator>().Play("Purple_win_character_anim");
                //return Purple_Sprites[i];
                break;
            case "cloth_5":
                chaaracter_anim.GetComponent<Animator>().Play("Yellow_win_character_anim");
                //return Yellow_Sprites[i];
                break;

            default:
                chaaracter_anim.GetComponent<Animator>().Play("Orange_win_character_anim");
                //return Sprites[i];
                break;

        }


    }

    public void lose_anim()
    {
        switch (PlayerPrefs.GetString("Cloth_Equip", "default"))
        {
            case "default":
                //chaaracter_anim.GetComponent<Animator>().Play("lose_charcter_anim");
                chaaracter_anim.GetComponent<Animator>().Play("Orange_lose_character_anim");
                //return Sprites[i];
                break;
            case "cloth_1":
                chaaracter_anim.GetComponent<Animator>().Play("Brown_lose_character_anim");
                //return Brown_Sprites[i];
                break;
            case "cloth_2":
                chaaracter_anim.GetComponent<Animator>().Play("Orange_lose_character_anim");
                //return Orange_Sprites[i];
                break;
            case "cloth_3":
                chaaracter_anim.GetComponent<Animator>().Play("Pink_lose_character_anim");
                //return Pink_Sprites[i];
                break;
            case "cloth_4":
                chaaracter_anim.GetComponent<Animator>().Play("Purple_lose_character_anim");
                //return Purple_Sprites[i];
                break;
            case "cloth_5":
                chaaracter_anim.GetComponent<Animator>().Play("Yellow_lose_character_anim");
                //return Yellow_Sprites[i];
                break;

            default:
                chaaracter_anim.GetComponent<Animator>().Play("Orange_lose_character_anim");
                //return Sprites[i];
                break;

        }


    }
}
